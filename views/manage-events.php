<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  <title>Manage Product</title>
</head>

<body>
  <?php include_once '../includes/navbar.php'; ?>
  <div class="container">
    <table class="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Description</th>
          <th>Date</th>
          <th>Time</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($events as $event) { ?>
          <tr>
            <td><?php echo $event->id(); ?></td>
            <td><?php echo $event->name(); ?></td>
            <td><?php echo $event->description(); ?></td>
            <td><?php echo $event->date(); ?></td>
            <td><?php echo $event->time(); ?></td>
            <td>
              <a href="?id=<?php echo $event->id(); ?>&action=edit-event">Edit</a>
              |
              <a href="?id=<?php echo $event->id(); ?>&action=delete-event">Delete</a>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>

</body>

</html>