<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <title>Create Booking</title>
</head>

<body>
    <?php include_once '../includes/navbar.php'; ?>
    <div class="container">
        <?php if (isset($_GET['message'])) {
            // Display the message
            echo '<div class="alert alert-danger">' . htmlspecialchars($_GET['message']) . '</div>';
        }
        ?>
        <form action="/index.php?action=create-booking" method="post">
            <div class="mb-3">
                <label for="event" class="form-label">Event:</label>
                <select class="form-control" id="event" name="event_id" required>
                    <?php foreach ($events as $event) { ?>
                        <option value="<?php echo $event->id(); ?>"><?php echo $event->name(); ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="mb-3">
                <label for="venue" class="form-label">Venue:</label>
                <select class="form-control" id="venue" name="venue_id" required>
                    <?php foreach ($venues as $venue) { ?>
                        <option value="<?php echo $venue->id(); ?>"><?php echo $venue->name(); ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="mb-3">
                <label for="date" class="form-label">Date:</label>
                <input type="date" class="form-control" id="date" name="date" required>
            </div>

            <div class="mb-3">
                <label for="time" class="form-label">Time:</label>
                <input type="time" class="form-control" id="time" name="time" required>
            </div>

            <div class="mb-3">
                <label for="status" class="form-label">Status:</label>
                <select class="form-control" id="status" name="status" required>
                    <option value="booked">Booked</option>
                    <option value="cancelled">Cancelled</option>
                </select>
            </div>

            <div class="mb-3">
                <label for="notes" class="form-label">Notes:</label>
                <textarea class="form-control" id="notes" name="notes"></textarea>
            </div>

            <input type="submit" class="btn btn-primary" value="Create Booking">
        </form>
    </div>

</body>

</html>