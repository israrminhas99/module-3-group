<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <title>Edit Event</title>
</head>

<body>
  <?php include_once '../includes/navbar.php'; ?>
  <div class="container">
    <?php if (isset($_GET['message'])) {
      // Display the message
      echo '<div class="alert alert-danger">' . htmlspecialchars($_GET['message']) . '</div>';
    }
    ?>
    <div class="container mt-3">
      <h1>Edit Event</h1>
      <form action="/index.php?action=update-event" method="POST">
        <input type="hidden" name="id" value="<?php echo $event->id() ?>">
        <div class="mb-3">
          <label for="name" class="form-label">Name</label>
          <input type="text" class="form-control" id="name" name="name" value="<?php echo $event->name() ?>">
        </div>
        <div class="mb-3">
          <label for="description" class="form-label">Description</label>
          <textarea class="form-control" id="description" name="description" rows="3"><?php echo $event->description() ?></textarea>
        </div>
        <div class="mb-3">
          <label for="date" class="form-label">Date</label>
          <input type="date" class="form-control" id="date" name="date" value="<?php echo $event->date() ?>">
        </div>
        <div class="mb-3">
          <label for="time" class="form-label">Time</label>
          <input type="time" class="form-control" id="time" name="time" value="<?php echo $event->time() ?>">
        </div>
        <button type="submit" class="btn btn-primary">Save Changes</button>
      </form>
    </div>
  </div>

</body>

</html>