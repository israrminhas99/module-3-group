<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <title>Manage Venues</title>
</head>

<body>
    <?php include_once '../includes/navbar.php'; ?>
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Capacity</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>

                <?php
                if ($venues) {
                    foreach ($venues as $venue) { ?>
                        <tr>
                            <td><?php echo $venue->id(); ?></td>
                            <td><?php echo $venue->name(); ?></td>
                            <td><?php echo $venue->description(); ?></td>
                            <td><?php echo $venue->capacity(); ?></td>
                            <td><?php echo $venue->address(); ?></td>
                            <td>
                                <a href="?id=<?php echo $venue->id(); ?>&action=edit-venue">Edit</a>
                                |
                                <a href="?id=<?php echo $venue->id(); ?>&action=delete-venue">Delete</a>
                            </td>
                        </tr>
                <?php }
                } ?>
            </tbody>
        </table>
    </div>

</body>

</html>