<?php

namespace Event\Factory;

use Event\Repository\BookingRepository;
use Event\Repository\BookingRepositoryFromPdo;

class BookingRepositoryFactory
{
    public static function make(): BookingRepository
    {
        $pdo = require __DIR__ . '/../../config/conn.php';
        return new BookingRepositoryFromPdo($pdo);
    }
}
