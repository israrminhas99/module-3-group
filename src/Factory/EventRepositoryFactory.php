<?php

namespace Event\Factory;

use Event\Repository\EventRepository;
use Event\Repository\EventRepositoryFromPdo;

class EventRepositoryFactory
{
    public static function make(): EventRepository
    {
        $pdo = require __DIR__ . '/../../config/conn.php';
        return new EventRepositoryFromPdo($pdo);
    }
}
