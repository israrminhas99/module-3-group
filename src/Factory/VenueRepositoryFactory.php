<?php

namespace Event\Factory;

use Event\Repository\VenueRepository;
use Event\Repository\VenueRepositoryFromPdo;

class VenueRepositoryFactory
{
    public static function make(): VenueRepository
    {
        $pdo = require __DIR__ . '/../../config/conn.php';
        return new VenueRepositoryFromPdo($pdo);
    }
}
