<?php

namespace Event\Action;

use Event\Factory\EventRepositoryFactory;
use Event\Factory\VenueRepositoryFactory;
use Event\Factory\BookingRepositoryFactory;

class HomeAction
{
    public function handle(): void
    {
        $repository = EventRepositoryFactory::make();
        $events = $repository->findAll();

        require_once __DIR__ . '/../../views/manage-events.php';
    }
    public function handletoManageVenues(): void
    {
        $repository = VenueRepositoryFactory::make();
        $venues = $repository->findAll();

        require_once __DIR__ . '/../../views/manage-venues.php';
    }
    public function handletoManageBooking(): void
    {
        $repository = BookingRepositoryFactory::make();
        $bookings = $repository->findAll();

        require_once __DIR__ . '/../../views/manage-bookings.php';
    }
    public function handleToAddNewEvent(): void
    {
        require_once __DIR__ . '/../../views/create-events.php';
    }
    public function handleToAddNewVenue(): void
    {
        require_once __DIR__ . '/../../views/create-venues.php';
    }
    public function handleToAddNewBooking(): void
    {
        $repository = VenueRepositoryFactory::make();
        $venues = $repository->findAll();
        $eventrepository = EventRepositoryFactory::make();
        $events = $eventrepository->findAll();
        require_once __DIR__ . '/../../views/create-bookings.php';
    }

    public function handleToEditEvent($id): void
    {
        $repository = EventRepositoryFactory::make();

        $event = $repository->find($id);

        require_once __DIR__ . '/../../views/edit-events.php';
    }
    public function handleToEditVenue($id): void
    {
        $repository = VenueRepositoryFactory::make();

        $venue = $repository->find($id);

        require_once __DIR__ . '/../../views/edit-venues.php';
    }
    public function handleToEditBooking($id): void
    {
        $venuesrepository = VenueRepositoryFactory::make();
        $venues = $venuesrepository->findAll();
        $eventrepository = EventRepositoryFactory::make();
        $events = $eventrepository->findAll();
        $repository = BookingRepositoryFactory::make();

        $booking = $repository->find($id);

        require_once __DIR__ . '/../../views/edit-bookings.php';
    }
}
