<?php

namespace Event\Action;

use Event\Entity\Venue;
use Event\Factory\VenueRepositoryFactory;

class VenueAction
{
    public function handle(): void
    {
        $name = filter_input(INPUT_POST, 'name');
        $description = filter_input(INPUT_POST, 'description');
        $capacity = filter_input(INPUT_POST, 'capacity', FILTER_VALIDATE_INT);
        $address = filter_input(INPUT_POST, 'address');

        $venue = new Venue(
            $name,
            $description,
            $capacity,
            $address
        );

        // Validate input fields
        $errors = [];
        if (empty($name)) {
            $errors[] = "Name is required.";
        }
        if (empty($description)) {
            $errors[] = "Description is required.";
        }
        if (empty($capacity)) {
            $errors[] = "Capacity is required.";
        }
        if (empty($address)) {
            $errors[] = "Address is required.";
        }

        // Display validation errors, if any
        if (!empty($errors)) {
            $error_message = implode("<br>", $errors);
            header('Location: ' . $_SERVER['HTTP_REFERER'] . '&message=' . urlencode($error_message));
            exit;
        }

        $repository = VenueRepositoryFactory::make();
        $repository->store($venue);

        header('Location: /manage-venues');
    }

    public function handleToUpdate(): void
    {
        $id = filter_input(INPUT_POST, 'id');
        $name = filter_input(INPUT_POST, 'name');
        $description = filter_input(INPUT_POST, 'description');
        $capacity = filter_input(INPUT_POST, 'capacity', FILTER_VALIDATE_INT);
        $address = filter_input(INPUT_POST, 'address');

        // Validate input fields
        $errors = [];
        if (empty($name)) {
            $errors[] = "Name is required.";
        }
        if (empty($description)) {
            $errors[] = "Description is required.";
        }
        if (empty($capacity)) {
            $errors[] = "Capacity is required.";
        }
        if (empty($address)) {
            $errors[] = "Address is required.";
        }

        // Display validation errors, if any
        if (!empty($errors)) {
            $error_message = implode("<br>", $errors);
            header('Location: ' . $_SERVER['HTTP_REFERER'] . '&message=' . urlencode($error_message));
            exit;
        }

        $repository = VenueRepositoryFactory::make();
        $venue = $repository->find($id);
        $venue->update($name, $description, $capacity, $address);
        $repository->update($venue);

        header('Location: /manage-venues');
    }

    public function handleToDelete($id): void
    {
        $repository = VenueRepositoryFactory::make();
        $repository->delete($id);

        header('Location: /manage-venues');
    }
}
