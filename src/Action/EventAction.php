<?php

namespace Event\Action;

use Event\Entity\Event;
use Event\Factory\EventRepositoryFactory;

class EventAction
{
    public function handle(): void
    {
        $name = filter_input(INPUT_POST, 'name');
        $description = filter_input(INPUT_POST, 'description');
        $date = filter_input(INPUT_POST, 'date');
        $time = filter_input(INPUT_POST, 'time');

        $event = new Event(
            $name,
            $description,
            $date,
            $time
        );
        // Validate input fields
        $errors = [];
        if (empty($name)) {
            $errors[] = "Name is required.";
        }
        if (empty($description)) {
            $errors[] = "Description is required.";
        }
        if (empty($date)) {
            $errors[] = "Date is required.";
        } else {
            // Validate date format
            $date_format = 'Y-m-d';
            $date_obj = \DateTime::createFromFormat($date_format, $date);
            if (!$date_obj || $date_obj->format($date_format) !== $date) {
                $errors[] = "Invalid date format. Date should be in the format 'yyyy-mm-dd'.";
            }
        }
        if (empty($time)) {
            $errors[] = "Time is required.";
        }


        // Display validation errors, if any
        if (!empty($errors)) {
            $error_message = implode("<br>", $errors);
            header('Location: ' . $_SERVER['HTTP_REFERER'] . '&message=' . urlencode($error_message));
            exit;
        }
        $repository = EventRepositoryFactory::make();
        $repository->store($event);

        header('Location: /manage-events');
    }
    public function handletoUpdate(): void
    {
        $id = filter_input(INPUT_POST, 'id');
        $name = filter_input(INPUT_POST, 'name');
        $description = filter_input(INPUT_POST, 'description');
        $date = filter_input(INPUT_POST, 'date');
        $time = filter_input(INPUT_POST, 'time');

        // Validate input fields
        $errors = [];
        if (empty($name)) {
            $errors[] = "Name is required.";
        }
        if (empty($description)) {
            $errors[] = "Description is required.";
        }
        if (empty($date)) {
            $errors[] = "Date is required.";
        } else {
            // Validate date format
            $date_format = 'Y-m-d';
            $date_obj = \DateTime::createFromFormat($date_format, $date);
            if (!$date_obj || $date_obj->format($date_format) !== $date) {
                $errors[] = "Invalid date format. Date should be in the format 'yyyy-mm-dd'.";
            }
        }
        if (empty($time)) {
            $errors[] = "Time is required.";
        }


        // Display validation errors, if any
        if (!empty($errors)) {
            $error_message = implode("<br>", $errors);
            header('Location: ' . $_SERVER['HTTP_REFERER'] . '&message=' . urlencode($error_message));
            exit;
        }

        $repository = EventRepositoryFactory::make();
        $event = $repository->find($id);
        $event->update($name, $description, $date, $time);
        $repository->update($event);

        header('Location: /manage-events');
    }
    public function handletoDelete($id): void
    {
        $repository = EventRepositoryFactory::make();
        $repository->delete($id);

        header('Location: /manage-events');
    }
}
