<?php

namespace Event\Action;

use Event\Entity\Booking;
use Event\Factory\BookingRepositoryFactory;

class BookingAction
{
    public function handle(): void
    {
        $event_id = filter_input(INPUT_POST, 'event_id', FILTER_VALIDATE_INT);
        $venue_id = filter_input(INPUT_POST, 'venue_id', FILTER_VALIDATE_INT);
        $date = filter_input(INPUT_POST, 'date');
        $time = filter_input(INPUT_POST, 'time');
        $status = filter_input(INPUT_POST, 'status');
        $notes = filter_input(INPUT_POST, 'notes');

        $booking = new Booking(
            $event_id,
            $venue_id,
            $date,
            $time,
            $status,
            $notes
        );

        $errors = [];
        if (empty($event_id)) {
            $errors[] = "Event ID is required.";
        }
        if (empty($venue_id)) {
            $errors[] = "Venue ID is required.";
        }
        if (empty($date)) {
            $errors[] = "Date is required.";
        }
        if (empty($time)) {
            $errors[] = "Time is required.";
        }

        if (!empty($errors)) {
            $error_message = implode("<br>", $errors);
            header('Location: ' . $_SERVER['HTTP_REFERER'] . '&message=' . urlencode($error_message));
            exit;
        }

        $repository = BookingRepositoryFactory::make();
        $repository->store($booking);

        header('Location: /manage-bookings');
    }
    public function handletoUpdate(): void
    {
        $id = filter_input(INPUT_POST, 'id');
        $event_id = filter_input(INPUT_POST, 'event_id', FILTER_VALIDATE_INT);
        $venue_id = filter_input(INPUT_POST, 'venue_id', FILTER_VALIDATE_INT);
        $date = filter_input(INPUT_POST, 'date');
        $time = filter_input(INPUT_POST, 'time');
        $status = filter_input(INPUT_POST, 'status');
        $notes = filter_input(INPUT_POST, 'notes');


        $errors = [];

        if (empty($event_id)) {
            $errors[] = "Event ID is required.";
        }
        if (empty($venue_id)) {
            $errors[] = "Venue ID is required.";
        }
        if (empty($date)) {
            $errors[] = "Date is required.";
        }
        if (empty($time)) {
            $errors[] = "Time is required.";
        }


        if (!empty($errors)) {
            $error_message = implode("<br>", $errors);
            header('Location: ' . $_SERVER['HTTP_REFERER'] . '&message=' . urlencode($error_message));
            exit;
        }

        $repository = BookingRepositoryFactory::make();
        $booking = $repository->find($id);
        $booking->update(
            $event_id,
            $venue_id,
            $date,
            $time,
            $status,
            $notes
        );
        $repository->update($booking);

        header('Location: /manage-bookings');
    }
    public function handletoDelete($id): void
    {
        $repository = BookingRepositoryFactory::make();
        $repository->delete($id);

        header('Location: /manage-bookings');
    }
}
