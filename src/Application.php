<?php

namespace Event;

use Event\Action\EventAction;
use Event\Action\VenueAction;
use Event\Action\BookingAction;
use Event\Action\HomeAction;

class Application
{
    public function run(): void
    {
        $action = filter_input(INPUT_GET, 'action');
        $id = filter_input(INPUT_GET, 'id');
        $total = filter_input(INPUT_GET, 'total');
        if ($action) {
            match ($action) {
                'create-event' => (new EventAction())->handle(),
                'update-event' => (new EventAction())->handletoUpdate(),
                'edit-event' => (new HomeAction())->handleToEditEvent($id),
                'delete-event' => (new EventAction())->handletoDelete($id),
                'create-venue' => (new VenueAction())->handle(),
                'update-venue' => (new VenueAction())->handletoUpdate(),
                'edit-venue' => (new HomeAction())->handleToEditVenue($id),
                'delete-venue' => (new VenueAction())->handletoDelete($id),
                'create-booking' => (new BookingAction())->handle(),
                'update-booking' => (new BookingAction())->handletoUpdate(),
                'edit-booking' => (new HomeAction())->handleToEditBooking($id),
                'delete-booking' => (new BookingAction())->handletoDelete($id),
                default => (new HomeAction())->handle(),
            };
        } else {
            $path = $_SERVER['REQUEST_URI'];
            $url = parse_url($path, PHP_URL_PATH);

            if ($url == '/manage-events') {
                (new HomeAction())->handle();
            } elseif ($url == '/add-new-event') {
                (new HomeAction())->handleToAddNewEvent();
            } elseif ($url == '/add-new-venue') {
                (new HomeAction())->handleToAddNewVenue();
            } elseif ($url == '/add-new-booking') {
                (new HomeAction())->handleToAddNewBooking();
            } elseif ($url == '/manage-venues') {
                (new HomeAction())->handleToManageVenues();
            } elseif ($url == '/manage-bookings') {
                (new HomeAction())->handleToManageBooking();
            }
            // else if($url=='/product-catalogue'){
            //     (new HomeAction())->handleToProductCatalogue();
            // }
            // else if($url=='/view-cart'){
            //     (new ViewCartAction())->handle();
            // }
            // else if($url=='/checkout'){
            //     (new OrdersAction())->handle();
            // }
            // else if($url=='/completed-orders'){
            //     (new OrdersAction())->handletoCompletedOrders();
            // }

            // else{
            //     (new HomeAction())->handleManageProducts();
            // }
        }
    }
}
