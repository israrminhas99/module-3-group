<?php

namespace Event\Repository;

use Event\Entity\Venue;
use PDO;

class VenueRepositoryFromPdo implements VenueRepository
{
    /** @see https://stitcher.io/blog/constructor-promotion-in-php-8 */
    public function __construct(private PDO $pdo)
    {
    }

    public function store(Venue $venue): void
    {
        $sql = $this->getStoreQuery($venue);
        $stm = $this->pdo->prepare($sql);

        $params = [
            ':name' => $venue->name(),
            ':address' => $venue->address(),
            ':capacity' => $venue->capacity(),
            ':description' => $venue->description()
        ];

        if ($venue->id()) {
            $params[':id'] = $venue->id();
        }

        $stm->execute($params);
    }

    public function update(Venue $venue): void
    {
        $sql = $this->getStoreQuery($venue);
        $stm = $this->pdo->prepare($sql);

        $params = [
            ':name' => $venue->name(),
            ':address' => $venue->address(),
            ':capacity' => $venue->capacity(),
            ':description' => $venue->description()
        ];

        if ($venue->id()) {
            $params[':id'] = $venue->id();
        }

        $stm->execute($params);
    }

    private function getStoreQuery(Venue $venue)
    {
        if ($venue->id()) {
            return <<<SQL
                UPDATE venues
                SET name=:name,
                    address=:address,
                    capacity=:capacity,
                    description=:description
                WHERE id=:id
            SQL;
        }
        return <<<SQL
            INSERT INTO venues (name, address, capacity, description)
            VALUES (:name, :address, :capacity, :description)
        SQL;
    }

    /** @return Venue[] */
    public function findAll(): array
    {
        $stm = $this->pdo->prepare(<<<SQL
            SELECT id, name, address, capacity, description
            FROM venues
        SQL);

        $stm->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Venue::class);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function find(int $id): Venue
    {
        $stm = $this->pdo->prepare(<<<SQL
            SELECT id, name, address, capacity , description
            FROM venues
            WHERE id=:id
        SQL);

        $stm->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Venue::class);
        $stm->bindParam(':id', $id);
        $stm->execute();

        return $stm->fetch();
    }

    public function delete(int $id): bool
    {
        $stm = $this->pdo->prepare(<<<SQL
            DELETE 
            FROM venues
            WHERE id=:id
        SQL);

        $stm->bindParam(':id', $id);
        return $stm->execute();
    }
}
