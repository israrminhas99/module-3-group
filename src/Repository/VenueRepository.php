<?php

namespace Event\Repository;

use Event\Entity\Venue;

interface VenueRepository
{
    public function store(Venue $venue): void;
    public function update(Venue $venue): void;
    public function delete(int $id): bool;

    /** @return Venue[] */
    public function findAll(): array;
    public function find(int $id): Venue;
}
