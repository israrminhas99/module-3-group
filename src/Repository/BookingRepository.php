<?php

namespace Event\Repository;

use Event\Entity\Booking;

interface BookingRepository
{
    public function store(Booking $booking): void;
    public function update(Booking $booking): void;
    public function delete(int $id): bool;

    /** @return Booking[] */
    public function findAll(): array;
    public function find(int $id): Booking;

    // public function findByEventId(int $eventId): array;
    // public function findByVenueId(int $venueId): array;
}
