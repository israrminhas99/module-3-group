<?php

namespace Event\Repository;

use Event\Entity\Event;
use PDO;

class EventRepositoryFromPdo implements EventRepository
{
    /** @see https://stitcher.io/blog/constructor-promotion-in-php-8 */
    public function __construct(private PDO $pdo)
    {
    }

    public function store(Event $event): void
    {
        $sql = $this->getStoreQuery($event);
        $stm = $this->pdo->prepare($sql);

        $params = [
            ':name' => $event->name(),
            ':description' => $event->description(),
            ':date' => $event->date(),
            ':time' => $event->time()
        ];

        if ($event->id()) {
            $params[':id'] = $event->id();
        }

        $stm->execute($params);
    }

    public function update(Event $event): void
    {
        $sql = $this->getStoreQuery($event);
        $stm = $this->pdo->prepare($sql);

        $params = [
            ':name' => $event->name(),
            ':description' => $event->description(),
            ':date' => $event->date(),
            ':time' => $event->time()
        ];

        if ($event->id()) {
            $params[':id'] = $event->id();
        }

        $stm->execute($params);
    }


    private function getStoreQuery(Event $event)
    {
        if ($event->id()) {
            return <<<SQL
                UPDATE events
                SET name=:name,
                    description=:description,
                    date=:date,
                    time=:time
                WHERE id=:id
            SQL;
        }
        return <<<SQL
            INSERT INTO events (name, description, date, time)
            VALUES (:name, :description, :date, :time)
        SQL;
    }

    /** @return Event[] */
    public function findAll(): array
    {
        $stm = $this->pdo->prepare(<<<SQL
            SELECT id, name, description, date, time
            FROM events
        SQL);

        $stm->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Event::class);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function find(int $id): Event
    {
        $stm = $this->pdo->prepare(<<<SQL
            SELECT id, name, description, date, time
            FROM events
            WHERE id=:id
        SQL);

        $stm->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Event::class);
        $stm->bindParam(':id', $id);
        $stm->execute();

        return $stm->fetch();
    }

    public function delete(int $id): bool
    {
        $stm = $this->pdo->prepare(<<<SQL
            DELETE 
            FROM events
            WHERE id=:id
        SQL);

        $stm->bindParam(':id', $id);
        return $stm->execute();
    }
}
