<?php

namespace Event\Repository;

use Event\Entity\Event;

interface EventRepository
{
    public function store(Event $event): void;
    public function update(Event $event): void;
    public function delete(int $id): bool;

    /** @return Event[] */
    public function findAll(): array;
    public function find(int $id): Event;
}
