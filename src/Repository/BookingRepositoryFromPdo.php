<?php

namespace Event\Repository;

use Event\Entity\Booking;
use PDO;

class BookingRepositoryFromPdo implements BookingRepository
{
    /** @see https://stitcher.io/blog/constructor-promotion-in-php-8 */
    public function __construct(private PDO $pdo)
    {
    }

    public function store(Booking $booking): void
    {
        $sql = $this->getStoreQuery($booking);
        $stm = $this->pdo->prepare($sql);

        $params = [
            ':event_id' => $booking->event_id(),
            ':venue_id' => $booking->venue_id(),
            ':date' => $booking->date(),
            ':time' => $booking->time(),
            ':status' => $booking->status(),
            ':notes' => $booking->notes(),
        ];

        if ($booking->id()) {
            $params[':id'] = $booking->id();
        }

        $stm->execute($params);
    }

    public function update(Booking $booking): void
    {
        $sql = $this->getStoreQuery($booking);
        $stm = $this->pdo->prepare($sql);

        $params = [
            ':event_id' => $booking->event_id(),
            ':venue_id' => $booking->venue_id(),
            ':date' => $booking->date(),
            ':time' => $booking->time(),
            ':status' => $booking->status(),
            ':notes' => $booking->notes(),
        ];

        if ($booking->id()) {
            $params[':id'] = $booking->id();
        }

        $stm->execute($params);
    }

    private function getStoreQuery(Booking $booking)
    {
        if ($booking->id()) {
            return <<<SQL
                UPDATE bookings
                SET event_id=:event_id,
                    venue_id=:venue_id,
                    date=:date,
                    time=:time,
                    status=:status,
                    notes=:notes
                WHERE id=:id
            SQL;
        }

        return <<<SQL
            INSERT INTO bookings (event_id, venue_id, date, time, status, notes)
            VALUES (:event_id, :venue_id, :date, :time, :status, :notes)
        SQL;
    }

    /** @return Booking[] */
    public function findAll(): array
    {
        $stm = $this->pdo->prepare(<<<SQL
        SELECT b.id, e.name as event_name, v.name as venue_name, b.date, b.time, b.status, b.notes
        FROM bookings b
        JOIN events e ON e.id = b.event_id
        JOIN venues v ON v.id = b.venue_id
    SQL);

        $stm->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Booking::class);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function find(int $id): Booking
    {
        $stm = $this->pdo->prepare(<<<SQL
            SELECT id, event_id, venue_id, date, time, status, notes
            FROM bookings
            WHERE id=:id
        SQL);

        $stm->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Booking::class);
        $stm->bindParam(':id', $id);
        $stm->execute();

        return $stm->fetch();
    }

    public function delete(int $id): bool
    {
        $stm = $this->pdo->prepare(<<<SQL
            DELETE 
            FROM bookings
            WHERE id=:id
        SQL);

        $stm->bindParam(':id', $id);
        return $stm->execute();
    }
}
