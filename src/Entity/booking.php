<?php

namespace Event\Entity;

use Event\Entity\Event;
use Event\Entity\Venue;

class Booking
{
    private ?int $id;
    private ?int $event_id;
    private ?int $venue_id;
    private ?string $date;
    private ?string $time;
    private ?string $status;
    private ?string $notes;

    public function __construct(
        int $event_id = null,
        int $venue_id = null,
        ?string $date = null,
        ?string $time = null,
        ?string $status = '',
        ?string $notes = null
    ) {
        $this->id = null;
        $this->event_id = $event_id;
        $this->venue_id = $venue_id;
        $this->date = $date;
        $this->time = $time;
        $this->status = $status;
        $this->notes = $notes;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function update(
        int $event_id,
        int $venue_id,
        ?string $date,
        ?string $time,
        string $status,
        ?string $notes = null
    ): void {
        $this->event_id = $event_id;
        $this->venue_id = $venue_id;
        $this->date = $date;
        $this->time = $time;
        $this->status = $status;
        $this->notes = $notes;
    }

    public function event_id(): ?int
    {
        return $this->event_id;
    }

    public function venue_id(): ?int
    {
        return $this->venue_id;
    }

    public function date(): ?string
    {
        return $this->date;
    }

    public function time(): ?string
    {
        return $this->time;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function notes(): ?string
    {
        return $this->notes;
    }
}
