<?php

namespace Event\Entity;

class Venue
{
    private ?int $id;
    private string $name;
    private ?string $description;
    private ?int $capacity;
    private ?string $address;

    public function __construct(
        string $name = '',
        ?string $description = null,
        ?int $capacity = null,
        ?string $address = null
    ) {
        $this->id = null;
        $this->name = $name;
        $this->description = $description;
        $this->capacity = $capacity;
        $this->address = $address;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function update(string $name, ?string $description, ?int $capacity, ?string $address): void
    {
        $this->name = $name;
        $this->description = $description;
        $this->capacity = $capacity;
        $this->address = $address;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function description(): ?string
    {
        return $this->description;
    }

    public function capacity(): ?int
    {
        return $this->capacity;
    }

    public function address(): ?string
    {
        return $this->address;
    }
}
