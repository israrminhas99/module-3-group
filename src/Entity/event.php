<?php

namespace Event\Entity;

class Event
{
    private ?int $id;
    private string $name;
    private ?string $description;
    private ?string $date;
    private ?string $time;

    public function __construct(
        string $name = '',
        ?string $description = null,
        ?string $date = null,
        ?string $time = null
    ) {
        $this->id = null;
        $this->name = $name;
        $this->description = $description;
        $this->date = $date;
        $this->time = $time;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function update(string $name, ?string $description, ?string $date, ?string $time): void
    {
        $this->name = $name;
        $this->description = $description;
        $this->date = $date;
        $this->time = $time;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function description(): ?string
    {
        return $this->description;
    }

    public function date(): ?string
    {
        return $this->date;
    }

    public function time(): ?string
    {
        return $this->time;
    }
}
