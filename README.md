## MODULE 3 GROUP
### Group Members
##### Israr Minhas
##### Kasun Wanigasekara
### Project Description
Event scheduling system: The event management system have  three tables  events, venues, and bookings. The events and venues tables would have a one-to-many relationship with the bookings table. Users could book a venue for an event, and a new booking record would be created in the bookings table, which would contain information about the event and venue.
### Tasks
#### Kasun Tasks
1. Database Design
2. CRUD operation for Venues
2. Frontend view for booking
#### Israr Tasks
1. Project startup and design
2. CRUD operation for Events
3. Backend work for Booking
