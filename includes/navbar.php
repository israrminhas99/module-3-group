<nav class='navbar navbar-expand-lg bg-light'>
  <div class='container-fluid'>
    <a class='navbar-brand' href='manage-events'>Event Management System</a>
    <button class='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
      <span class='navbar-toggler-icon'></span>
    </button>
    <div class='collapse navbar-collapse' id='navbarSupportedContent'>
      <ul class='navbar-nav me-auto mb-2 mb-lg-0'>

        <li class='nav-item'>
          <a class='nav-link' href='manage-events'>Manage events</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link' href='add-new-event'>Add new event</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link' href='manage-venues'>Manage venues</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link' href='add-new-venue'>Add new venues</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link' href='manage-bookings'>Manage bookings</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link' href='add-new-booking'>Add new bookings</a>
        </li>


      </ul>

    </div>
  </div>
</nav>